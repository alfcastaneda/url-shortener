import {App, shortUrl, redirect} from './app.js';

//Since I will just be using this I just attached it to window object.
window.shortUrl = shortUrl;
window.redirect = redirect;
