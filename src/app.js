import './app.css';
const App = (id) => fetch(`/url?id=${id}`);

const shortUrl = (event) => {
  event.preventDefault();
  const form = event.target;
  const url = form[0].value;
  form.reset();
  return fetch('/url', {
    method: 'post',
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify({url})
  })
    .catch(e => console.error(e))
    .then(response => response.json())
    .then(({shortUrl}) =>  {
      const link = document.querySelector('#shortenedURL');
      link.innerHTML = `<span onclick="redirect('${shortUrl}')">${shortUrl}</span>`;
    });
};

const redirect = (shortenedUrl) => {
  return fetch(`/url?shortUrl=${shortenedUrl}`, {
    method: 'get',
  })
    .then(r => r.json())
    .then(({url}) => {
      window.open(url, '_blank');
    });
}


export {App, shortUrl, redirect};
