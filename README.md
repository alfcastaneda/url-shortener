# URL Shortener #

This is a very basic implementation of an url shortener using express and mongodb

### What is this repository for? ###

*Just input a url and get a short version interpreted by node and redirected on the FE.

### How do I get set up? ###

* git clone repo
* npm install
* npm run serve
* open browser on localhost:3000


### Who do I talk to? ###

* contact me at alfcastaneda@gmail.com