const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const fs = require('fs');

const app = express();
const configFn = require('./webpack.config.js');
const config = configFn();
const compiler = webpack(config);
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const URL = require('./models/Url');
const baseShortURL = 'http://shrturl.xyz/';
mongoose.Promise = Promise;

const alphabet = "123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
const base = alphabet.length;

function encode(num){
  var encoded = '';
  while (num) {
    let characterIndex = num % base;
    num = Math.floor(num / base);
    encoded = alphabet.charAt(characterIndex) + encoded;
  }
  return encoded;
}

function decode(str){
  var decoded = 0;
  while (str){
    var index = alphabet.indexOf(str[0]);
    var power = str.length - 1;
    decoded += index * (Math.pow(base, power));
    str = str.substring(1);
  }
  return decoded;
}


app.use(require("webpack-dev-middleware")(compiler, {
  noInfo: true, publicPath: config.output.publicPath
}));

app.use(require("webpack-hot-middleware")(compiler, {
  log: console.log, path: '/__webpack_hmr', heartbeat: 10 * 1000
}));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/url', function (req, res) {
  const encodedId = req.query.shortUrl.split(baseShortURL)[1];
  const decodedId = decode(encodedId);
  URL.findOne({_id: decodedId}, (err, urlResult) => {
    if (err) {
      console.error('Error saving post', error);
      return res.status(500).send({message: 'Internal error, URL not found'});
    }
    const {url} = urlResult;
    res.send({url});
  })
  
});

app.post('/url', (req, res) => {
  const longURL = req.body.url;
  URL.findOne({url: longURL}, (err, result) => {
    if (result) {
      const id = result._id;
      const encodedStr = encode(id);
      res.send({'shortUrl': `${baseShortURL}${encodedStr}`});
    } else {
      const urlModel = new URL({url: longURL});
      urlModel.save((err, result) => {
        if (err) {
          console.error('Error saving post', error);
          return res.status(500).send({message: 'Saving post error'});
        }
        const id = result._id;
        const encodedStr = encode(id);
        res.status(200).send({shortUrl: `${baseShortURL}${encodedStr}`});
      });
    }

  });
})

// Serve the files on port 3000.
app.listen(3000, function () {
  console.log('Example app listening on port 3000!\n');
});

mongoose.connect('mongodb://testUser:testingdb@ds139585.mlab.com:39585/tests', {useMongoClient: true}, (err) => {
  if(!err)
    console.log('connected');
});

